<!DOCTYPE html>
<html lang="fi">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
</head>
<body>
<div id="container">
	<h1><?php echo $title; ?></h1>
	<h2><?php echo $header1; ?></h2>

	<div id="body">
			<?php
			echo form_open('user/create');
			?>
			<label>Nimi:</label>
			<?php echo form_input('nimi') ?>
			<br />
			<lable>Tekijä:</lable>
			<?php echo form_input('tekija') ?>
			<br />
			<lable>Kommentti:</lable>
			<?php echo form_textarea('kommentti') ?>
			<input type="submit" name= "submit" value= "Lisää" />
			<?php
			echo form_close(); 
			?>
			
			<h2><?php echo $header2; ?></h2>
			<?php
			foreach($books as $object){
				echo 'Kirjan nimi: ' . $object->Nimi . 
				', Kirjan tekijä: ' . $object->Tekija . 
				', Kommenttisi: ' . $object->Kommentti . '<br/>';
			}
			?>
			
			<h2><?php echo $header3; ?></h2>
			<?php
			echo form_open('user/delete');
			?>
			<label>Kirjan nimi:</label>
			<?php echo form_input('nimi') ?>
			<lable>Tekijä:</lable>
			<?php echo form_input('tekija') ?>
			<input type="submit" name= "delete" value= "Poista" />
			<br />
			<?php
			echo form_close(); 
			?>
			
			<h2><?php echo $header4; ?></h2>
			<?php
			echo form_open('user/update');
			?>
			<label>Kirjan nimi:</label>
			<?php echo form_input('nimi') ?>
			<lable>Tekijä:</lable>
			<?php echo form_input('tekija') ?>
			<br />
			<lable>Uusi nimi:</lable>
			<?php echo form_input('unimi') ?>
			<lable>Uusi tekijä:</lable>
			<?php echo form_input('utekija') ?>
			<br />
			<lable>Uusi kommentti:</lable>
			<?php echo form_textarea('ukommentti') ?>
			<input type="submit" name= "update" value= "Päivitä" />
			<br />
			<?php
			echo form_close(); 
			?>
			</div>
	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>
</body>
</html>