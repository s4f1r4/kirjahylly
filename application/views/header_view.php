<!DOCTYPE html>
<html lang="fi">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <title><?php echo (isset($title)) ? $title : "Kirjahylly" ?> </title>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/css/style.css" />
</head>
<body>
	<?php 
	if($this->session->userdata('user_name'))
	{
		echo anchor('user/logout', 'Kirjaudu ulos');
	}
	else
	{
		echo form_open("user/login"); ?>
		<label for="email">Sähköposti:</label>
		<input type="text" id="email" name="email" value="" />
		<label for="pass">Salasana:</label>
		<input type="password" id="pass" name="pass" value="" />
		<input type="submit" class="" value="Kirjaudu sisään" />
		<?php echo form_close();
	}
	 ?>
 <div id="wrapper">