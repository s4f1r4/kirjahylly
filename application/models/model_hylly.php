<?php

class Model_hylly extends CI_Model{
		
	function _construct (){
		parent::_construct();
	}
	
	function getKirjanTiedot($user_name){
		$query = "SELECT * FROM kirjahylly WHERE username = '$user_name'";
		$result = $this->db->query($query);
		if($result->num_rows() > 0){
			return $result->result();
		}else{
			return NULL;
		}
	}
	
	function insertKirjanTiedot($nimi, $tekija, $kommentti, $user_name){
		$this->db->set('Nimi', $nimi);
		$this->db->set('Tekija', $tekija);
		$this->db->set('Kommentti', $kommentti);
		$this->db->set('username', $user_name);
		$query = $this->db->insert('kirjahylly');
		return;
	}
	
	function deleteKirjanTiedot($nimi, $tekija, $user_name){
		$query = "DELETE FROM kirjahylly 
		WHERE Nimi = '$nimi' AND Tekija = '$tekija' AND username = '$user_name'";
		$result = $this->db->query($query);
		return;
	}
	
	function updateKirjanTiedot($vnimi, $vtekija, $unimi, $utekija, $ukommentti, $user_name){
		$query = "UPDATE kirjahylly 
		SET Nimi = '$unimi', Tekija = '$utekija', Kommentti = '$ukommentti'
		WHERE Nimi = '$vnimi' AND Tekija = '$vtekija' AND username = '$user_name'";
		$result = $this->db->query($query);
		return;
	}
}
