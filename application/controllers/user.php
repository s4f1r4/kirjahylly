<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller{
 	public function __construct()
 	{
  		parent::__construct();
  		$this->load->model('model_user');
  		$this->load->model('model_hylly');
 	}
 
 	public function index()
 	{
  		if(($this->session->userdata('user_name')!=""))
  		{
   			$this->welcome();
  		}
  		else
  		{
   			$data['title']= 'Kirjahylly';
		
   			$this->load->view('header_view',$data);
   			$this->load->view("registration_view.php", $data);
   			$this->load->view('footer_view',$data);
  		}
 	}
 
 	public function welcome()
 	{
  		$data['title']= 'Tervetuloa Kirjahyllyyn!';
  		$data['header1'] = 'Lisää lempikirjasi ja arvioi ne:';
  		$data['header1'] = 'Lisää lempikirjasi ja arvioi ne:';
		$data['header2'] = 'Kaikki kirjasi:';
		$data['header3'] = 'Poista kirja kirjahyllystä:';
		$data['header4'] = 'Muokkaa kirjan tietoja:';
		$user_name = $this->session->userdata('user_name');
		$data['books'] = $this->model_hylly->getKirjanTiedot($user_name);
  
  		$this->load->view('header_view',$data);
  		$this->load->view('profile_view.php', $data);
  		$this->load->view('footer_view',$data);
 	}
 
 	public function lisays(){
 		$this->load->view('welcome_message');
	}
	 
	public function create(){
		$nimi = $this->input->post('nimi');
		$tekija = $this->input->post('tekija');
		$kommentti = $this->input->post('kommentti');
		$user_name = $this->session->userdata('user_name');
		
		$this->load->model('model_hylly');
		$this->model_hylly->insertKirjanTiedot($nimi, $tekija, $kommentti, $user_name);
		
		$this->load->view('tallennus_onnistui');
	}
	
	public function delete(){
		$nimi = $this->input->post('nimi');
		$tekija = $this->input->post('tekija');
		$user_name = $this->session->userdata('user_name');
		
		$this->load->model('model_hylly');
		$this->model_hylly->deleteKirjanTiedot($nimi, $tekija, $user_name);
		
		$this->load->view('tallennus_onnistui');
	}
	
	public function update(){
		$vnimi = $this->input->post('nimi');
		$vtekija = $this->input->post('tekija');
		$unimi = $this->input->post('unimi');
		$utekija = $this->input->post('utekija');
		$ukommentti = $this->input->post('ukommentti');
		$user_name = $this->session->userdata('user_name');
		
		$this->load->model('model_hylly');
		$this->model_hylly->updateKirjanTiedot($vnimi, $vtekija, $unimi, $utekija, 
		$ukommentti, $user_name);
		
		$this->load->view('tallennus_onnistui');
		
	}
 
 	public function login()
 	{
  		$email=$this->input->post('email');
  		$password=md5($this->input->post('pass'));

  		$result=$this->model_user->login($email,$password);
  		if($result)
  		{
  			$this->welcome();
		}
  		else
  		{
  			$this->index();
		}
 	}
 
 	public function thank()
 	{
  		$data['title']= 'Kiitos käynnistä!';
  		$this->load->view('header_view',$data);
  		$this->load->view('thank_view.php', $data);
  		$this->load->view('footer_view',$data);
 	}
 
 	public function registration()
 	{
  		$this->load->library('form_validation');
  		
  		$this->form_validation->set_rules('user_name', 'User Name', 
  		'trim|required|min_length[4]|xss_clean');
  		$this->form_validation->set_rules('email_address', 'Your Email', 
  		'trim|required|valid_email');
  		$this->form_validation->set_rules('password', 'Password', 
  		'trim|required|min_length[4]|max_length[32]');
  		$this->form_validation->set_rules('con_password', 'Password Confirmation', 
  		'trim|required|matches[password]');

  		if($this->form_validation->run() == FALSE)
  		{
   			$this->index();
  		}
  		else
  		{
   			$this->model_user->add_user();
   			$this->thank();
 		}
 	}
 
 	public function logout()
 	{
  		$newdata = array(
  		'user_id'   =>'',
  		'user_name'  =>'',
  		'user_email'     => '',
  		'logged_in' => FALSE,
  		);
  		$this->session->unset_userdata($newdata );
  		$this->session->sess_destroy();
  		$this->index();
 	}
}
?>