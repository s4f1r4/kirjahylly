<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->home();
	}
	
	public function home(){
		$this->load->model('model_hylly');
		$this->load->model('user_model');

		$data['title'] = 'Kirjahylly';
		$data['header1'] = 'Lisää lempikirjasi ja arvioi ne:';
		$data['header2'] = 'Kaikki kirjasi:';
		$data['header3'] = 'Poista kirja kirjahyllystä:';
		$data['header4'] = 'Muokkaa kirjan tietoja:';
		$data['books'] = $this->model_hylly->getKirjanTiedot();

		$this->load->view('welcome_message', $data);
	}
	
	public function lisays(){
		$this->load->view('welcome_message');
	}
	
	public function create(){
		$nimi = $this->input->post('nimi');
		$tekija = $this->input->post('tekija');
		$kommentti = $this->input->post('kommentti');
		
		$this->load->model('model_hylly');
		$this->model_hylly->insertKirjanTiedot($nimi, $tekija, $kommentti);
		
		$this->load->view('tallennus_onnistui');
	}
	
	public function delete(){
		$nimi = $this->input->post('nimi');
		$tekija = $this->input->post('tekija');
		
		$this->load->model('model_hylly');
		$this->model_hylly->deleteKirjanTiedot($nimi, $tekija);
		
		$this->load->view('tallennus_onnistui');
	}
	
	public function update(){
		$vnimi = $this->input->post('nimi');
		$vtekija = $this->input->post('tekija');
		$unimi = $this->input->post('unimi');
		$utekija = $this->input->post('utekija');
		$ukommentti = $this->input->post('ukommentti');
		
		$this->load->model('model_hylly');
		$this->model_hylly->updateKirjanTiedot($vnimi, $vtekija, $unimi, $utekija, $ukommentti);
		
		$this->load->view('tallennus_onnistui');
		
}


}
